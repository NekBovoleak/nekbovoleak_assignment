/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation'
import Quiz from './src/Quiz';
import Results from './src/Results';
import testCard from './src/testCard';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class App extends Component {
  render() {
    return (
      <AppNavigator/>
    );
  }
}

const AppNavigator = createAppContainer(createStackNavigator({
    QuizScreen: {
      screen: Quiz,
      navigationOptions: {
        header: null
      }
    },
    ResultsScreen: {
      screen: Results
    },
    CardScreen: {
      screen: testCard,
      navigationOptions: {
        title: 'Card'
      }
    }
}))
