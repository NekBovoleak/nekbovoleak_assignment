import React, {Component} from 'react'
import {View, StyleSheet, Text, TouchableOpacity, Image,ImageBackground} from 'react-native'

class Quiz extends Component {
    render() {
        console.log(this.state)
        return(
            <ImageBackground 
                source={require('./img/front1.jpg')}
                style={styles.backgroundImage}>
                <View style={{flex : 1, justifyContent: 'center', alignItems: 'center'}}>
                {/* <Image source={{uri: 'http://hdlatestwallpaper.com/wp-content/uploads/2018/07/Purple-galaxy-HD-wallpaper-New.jpg'}}
                       style={styles.backgroundImage}> */}
                <Image source={require('./img/logo.png')}
                       style={{width: 150, height: 150, marginBottom: 15}} />
                <Text style={styles.titleText}>Brain Challenge</Text>
                <TouchableOpacity style={styles.buttonStyle} onPress= {() => this.props.navigation.navigate('CardScreen')}>
                    <Text style={styles.buttonText}>Start</Text>
                </TouchableOpacity>
                </View>
            </ImageBackground>
            
        );
    }
}

const styles = StyleSheet.create({
    titleText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 28,
        marginBottom: 15
    },
    buttonStyle: {
        width: '50%',
        height: 40,
        backgroundColor: '#9f005d',
        borderRadius: 15,
        marginTop: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 17,
        textAlign: 'center',
        margin: 5,
        paddingTop: 3
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    }
})

export default Quiz;