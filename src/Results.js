import React from 'react'
import {Text, View,StyleSheet,ImageBackground, Image} from 'react-native'

class Results extends React.Component {

    render() {
        const totalQuestion = this.props.navigation.state.params.totalQuestion;
        const score = this.props.navigation.state.params.tScore;
        let overallScore = (score/totalQuestion).toFixed(2) * 100;
        return(
            <ImageBackground
                source={require('./img/background3.jpg')}
                style={styles.backgroundImage}>
                <View style={{flex : 1, justifyContent: 'center', alignItems: 'center', marginBottom: 150}}>
                    <Image source={require('./img/success.png')}
                       style={{width: 150, height: 150, marginBottom: 15}} />
                    <Text style={{fontSize: 25}}>Result Screen</Text>
                    <Text style={styles.styleText}>Total Score is: {score}/{totalQuestion}</Text>
                    <Text style={styles.styleText}>Total Overall Point is: {overallScore}%</Text>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
    styleText: {
        fontSize: 15,
    }
})

export default Results;