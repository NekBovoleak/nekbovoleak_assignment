import React, { Component } from "react";
import {
    Container,
    Content,
    Card,
    CardItem,
    Body,
    Text,
    ListItem,
    Left,
    Right,
    Radio,
    Button,
    
} from "native-base";
import { View,StyleSheet,ImageBackground } from "react-native";

const questions = [
    {
        text: "What is the name of the US president?",
        correct: 1,
        answers: ["Obama", "Trump", "Roosvelt", "Putin"]
    },
    {
        text: "What is the square root of 100?",
        correct: 3,
        answers: ["sin(10)", "1", "100", "10"]
    },
    {
        text: "Which is the largest planet in the solar system?",
        correct: 0,
        answers: ["Jupiter", "Neptune", "Earth", "Mars"]
    },
    {
        text: "How many Presidents have there been of the USA? (Updated 2014)",
        correct: 3,
        answers: ["36", "29", "46", "44"]
    },
    {
        text: "How many men have walked on the moon?",
        correct: 2,
        answers: ["10", "5", "12", "9"]
    },
];

export default class testCard extends Component {
    constructor(props) {
        super(props);
        this.score = 0
        this.state={
            radioSelected: false,
            answer:[],
        }
    }

    handleButtonSelect(indexQuestion, indexAnswer,correctAnswer) {
        
        let userPick = this.state.answer 
        userPick[indexQuestion]=indexAnswer;
        this.setState={
            answer:userPick,  
        }
        console.log("Question = ",indexQuestion,"Answer = ",indexAnswer)
        console.log("UserPick ==> ",userPick[indexQuestion])
        this.forceUpdate();
        
    }

    handleResult() {
      let allResult = this.state.answer
      var totalScore = 0;
      for(var i=0;i<questions.length;i++){
        if(questions[i].correct === allResult[i]) totalScore += 1;
      }
      console.log("Total Score ===> " + totalScore);
      console.log(allResult);

      this.props.navigation.navigate('ResultsScreen',{
        tScore: totalScore,
        totalQuestion: questions.length
      });
    }

    componentWillMount() {
        console.log(questions);
    }

    render() {
        return (
                <Container>
                <ImageBackground source={require('./img/background1.jpg')}
                style={styles.backgroundImage}>
                <Content style={{ paddingLeft: 15, paddingRight: 15, paddingTop: 10 }}>
                    {questions.map((all, indexQuestion = index) => {
                        return (
                            <Card key={indexQuestion} style={{marginTop:25}}>
                                <CardItem>
                                    <Body>
                                    <View>
                                        <Text>{all.text}</Text>
                                        {all.answers.map((ans, indexAnswer = index) => {
                                            return (
                                                <View key={indexAnswer}>
                                                    {/* {console.log("Index Card = ",indexQuestion,"Display index = ",indexAnswer," Value = ",ans)} */}
                                                    <ListItem selected={this.state.radioSelected}
                                                              onPress={() => this.handleButtonSelect(indexQuestion,indexAnswer)}>
                                                        <Left>
                                                            <Text>{ans}</Text>
                                                            
                                                        </Left>
                                                        <Right>
                                                            <Radio
                                                                selected={this.state.answer[indexQuestion]===indexAnswer?true:false}
                                                                onPress={() => this.handleButtonSelect(indexQuestion,indexAnswer)}
                                                                
                                                            />
                                                        </Right>
                                                    </ListItem>
                                                </View>
                                            );
                                        })}
                                    </View>
                                    </Body>
                                </CardItem>
                            </Card>
                        );
                    })}
                    <Button style={styles.buttonStyle} block success onPress= {() => this.handleResult()}><Text> Finish </Text></Button>
                </Content>
                </ImageBackground>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
    buttonStyle: {
        marginBottom: 15,
        marginTop: 15
    }
})

