/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
// import testCard from './src/testCard'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
